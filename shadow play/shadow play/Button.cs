﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.IO;
using Microsoft.Win32;
using System.Windows;
using kinect.display;


namespace shadowplay

{
    class Button
    {
        Texture2D image;
        SpriteFont font;
        Rectangle location;
        string text;
        Vector2 textLocation;
        SpriteBatch spriteBatch;
        MouseState mouse;
        MouseState oldMouse;
        bool clicked = false;
        string clickText = "Button was Clicked!";
        string label;
        string name;
        KinectRecorder recorder;

        public Button(Texture2D texture, SpriteFont font, SpriteBatch sBatch)
        {
            image = texture;
            this.font = font;
            location = new Rectangle(0, 0, image.Width, image.Height);
            spriteBatch = sBatch;
        }

        public string Name
        {
            get { return name; }
            set { name = value ; }
        }

        public string Text
        {
            get { return text; }
            set
            { 
                text = value;
                Vector2 size = font.MeasureString(text);
                textLocation = new Vector2();
                textLocation.Y = location.Y + ((image.Height / 2) - (size.Y / 2));
                textLocation.X = location.X + ((image.Width / 2) - (size.X / 2));
            }
        }

        public void Location(int x, int y)
        {
            location.X = x;
            location.Y = y;
        }

        public void setLabel(string nlabel)
        {
            this.label = nlabel;

        }

        public void Update()
        {
            mouse = Mouse.GetState();

            if (mouse.LeftButton == ButtonState.Released &&
                oldMouse.LeftButton == ButtonState.Pressed)
            {
                if (location.Contains(new Point(mouse.X, mouse.Y)))
                {
                    clicked = true;
                }
            }

            if (clicked)
            {
                if (Name == "Record")
                {
                    record();
                }
            }
           

            Text = this.label;

            oldMouse = mouse;
        }
        void record()
        {
            if (recorder != null)
            {
                StopRecord();
                return;
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog { Title = "Select filename", Filter = "Replay files|*.replay" };

            if (saveFileDialog.ShowDialog() == true)
            {
                DirectRecord(saveFileDialog.FileName);
            }

        }

        void DirectRecord(string targetFileName)
        {
            Stream recordStream = File.Create(targetFileName);
            recorder = new KinectRecorder(KinectRecordOptions.Skeletons | KinectRecordOptions.Color, recordStream);
            // record.Content = "Stop Recording";
        }

        void StopRecord()
        {
            if (recorder != null)
            {
                recorder.Stop();
                recorder = null;
                // record.Content = "Record";
                return;
            }
        }


        public void Draw()
        {
            spriteBatch.Begin();

            if (location.Contains(new Point(mouse.X, mouse.Y)))
            {
                spriteBatch.Draw(image,
                    location,
                    Color.Silver);
            }
            else
            {
                spriteBatch.Draw(image,
                    location,
                    Color.White);
            }


            spriteBatch.DrawString(font, text, textLocation, Color.Black);

            if (clicked)
            {
                Vector2 position = new Vector2(10, 200);
                if (this.label == "button1")
                {
                    clickText = this.label;
                }
               
                spriteBatch.DrawString(font, clickText, position, Color.White);
            }

            spriteBatch.End();
        }
    }
}
