﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace kinect.display

{
    public class ReplaySkeletonFrameReadyEventArgs : EventArgs
    {
        public ReplaySkeletonFrame SkeletonFrame { get; set; }
    }
}

